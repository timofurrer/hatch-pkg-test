# hatch-pkg-test

[![PyPI - Version](https://img.shields.io/pypi/v/hatch-pkg-test.svg)](https://pypi.org/project/hatch-pkg-test)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/hatch-pkg-test.svg)](https://pypi.org/project/hatch-pkg-test)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install hatch-pkg-test
```

## License

`hatch-pkg-test` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
