# SPDX-FileCopyrightText: 2023-present Timo Furrer <tfurrer@gitlab.com>
#
# SPDX-License-Identifier: MIT
__version__ = "0.0.1"
